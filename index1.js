console.log (1);
console.log (2);

// setTimeout (() => {
//     console.log ('callback function fired');
// }, 2000)

const res = ((cb, num) => {
    let val = num % 2 === 0 ? 'even' : 'odd';
    cb (val);
})

console.log (3);
console.log (4);

const callback = (num) => {
    console.log (num);
}

res (callback, 4);