const url = 'https://jsonplaceholder.typicode.com/photos';

const todo = ((url) => {
    const request = new XMLHttpRequest();

    request.addEventListener ('readystatechange', () => {
        // console.log (request, request.readyState);
        if (request.readyState === 4 && request.status === 200) {
            console.log (request.responseText);
        }else if (request.readyState === 4) {
            console.log ('Not find the data');
        }
    })

    request.open ('GET', url);
    request.send ();
})

todo (url);