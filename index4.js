const url = 'https://jsonplaceholder.typicode.com/photos';

const getData = ((callback) => {

    const request = new XMLHttpRequest();

    request.addEventListener ('readystatechange', () => {
        // console.log (request, request.readyState);
        if (request.readyState === 4 && request.status === 200) {
            // console.log (request.responseText);
            callback (undefined, request.responseText);
        }else if (request.readyState === 4) {
            // console.log ('Not found the data');
            callback ('Not found the data', undefined);
        }
    })

    request.open ('GET', url);
    request.send ();
})

console.log ('hello');
getData ((err, data) => {
    if (err) {
        console.log (err);
    }else {
        console.log (data);
    }
})
console.log ('hi');