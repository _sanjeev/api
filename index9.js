const getData = ((filepath) => {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();

        request.addEventListener('readystatechange', () => {
            if (request.readyState === 4 && request.status === 200) {
                const data = JSON.parse (request.responseText);
                resolve (data);
            }else if (request.readyState === 4) {
                reject ('Not Found the data');
            }
        })
        request.open('GET', filepath);
        request.send();
    })
})

getData ('https://jsonplaceholder.typicode.com/todos').then ((data) => {
    console.log (data);
    return getData ('https://jsonplaceholder.typicode.com/todos');
}).then ((data) => {
    console.log (data);
    return getData ('https://jsonplaceholder.typicode.com/todos');
}).then ((data) => {
    console.log (data);
})
.catch ((err) => {
    console.log (err);
});