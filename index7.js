const getData = ((filePath) => {
    return new Promise ((resolve, reject) => {
        fetch (filePath).then ((response) => {
            return response.json();
        }).then ((data) => {
            resolve (data);
        }). catch ((e) => {
            reject (e);
        })
    })
})

const file = 'https://jsonplaceholder.typicode.com/users';

getData (file)
.then ((res) => {
    console.log (res);
})
.then ((data) => {
    console.log (data);
});