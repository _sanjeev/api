
const getData = ((filepath, callback) => {
    const request = new XMLHttpRequest();

    request.addEventListener('readystatechange', () => {
        // console.log (request,request.readyState);
        if (request.readyState === 4 && request.status === 200) {
            // console.log (request.responseText);
            const data = JSON.parse(request.responseText);
            callback(undefined, data);
        } else if (request.readyState === 4) {
            // console.log ('Not data found');
            callback('Not found the data', undefined);
        }
    })

    request.open('GET', filepath);
    request.send();
})

getData('https://jsonplaceholder.typicode.com/users', (err, data) => {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
    getData('https://jsonplaceholder.typicode.com/users', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
        }
        getData('https://jsonplaceholder.typicode.com/users', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log(data);
            }
        });
    });
});