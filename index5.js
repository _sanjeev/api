const url = 'https://jsonplaceholder.typicode.com/photos';

const data = ((callback) => {

    const request = new XMLHttpRequest();
    request.addEventListener ('readystatechange', () => {
        // console.log (request, request.readyState);
        if (request.readyState === 4 && request.status === 200) {
            // console.log (request.responseText);
            const data = JSON.parse (request.responseText);
            callback (undefined, data);
        }else if (request.readyState === 4){
            // console.log ('Not found the data');
            callback ('Not found the data', undefined);
        }
    })
    request.open ('GET', url);
    request.send ();
})

data ((err, data) => {
    if (err) {
        console.log (err);
    }else {
        console.log (data);
    }
})